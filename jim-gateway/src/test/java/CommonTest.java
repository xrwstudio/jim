import com.jd.jim.gateway.kit.NetAddressIsReachable;
import org.junit.Test;

/**
 * @author ：sizegang
 * @description：
 * @version:  1.0
 */
public class CommonTest {

    @Test
    public void test() {
        boolean reachable = NetAddressIsReachable.checkAddressReachable("127.0.0.1", 11211, 1000);
        System.out.println(reachable);
    }


}
