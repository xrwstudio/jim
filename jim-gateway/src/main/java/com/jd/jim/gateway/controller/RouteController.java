package com.jd.jim.gateway.controller;

import com.jd.jim.gateway.service.AccountService;
import com.jd.jim.gateway.service.CommonBizService;
import com.jd.jim.gateway.service.UserInfoCacheService;
import com.jd.jim.common.enums.StatusEnum;
import com.jd.jim.common.exception.JIMException;
import com.jd.jim.common.pojo.RouteInfo;
import com.jd.jim.common.pojo.JIMUserInfo;
import com.jd.jim.common.res.BaseResponse;
import com.jd.jim.common.res.NULLBody;
import com.jd.jim.common.route.algorithm.RouteHandle;
import com.jd.jim.common.util.RouteInfoParseUtil;
import com.jd.jim.gateway.api.RouteApi;
import com.jd.jim.gateway.api.vo.req.ChatReqVO;
import com.jd.jim.gateway.api.vo.req.LoginReqVO;
import com.jd.jim.gateway.api.vo.req.P2PReqVO;
import com.jd.jim.gateway.api.vo.req.RegisterInfoReqVO;
import com.jd.jim.gateway.api.vo.res.RegisterInfoResVO;
import com.jd.jim.gateway.api.vo.res.JIMServerResVO;
import com.jd.jim.gateway.cache.ServerCache;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;
import java.util.Set;

/**
 * @author ：sizegang
 * @description：
 * @version: 1.0
 */
@Controller
@RequestMapping("/")
@Slf4j
public class RouteController implements RouteApi {

    @Autowired
    private ServerCache serverCache;

    @Autowired
    private AccountService accountService;

    @Autowired
    private UserInfoCacheService userInfoCacheService;

    @Autowired
    private CommonBizService commonBizService;

    @Autowired
    private RouteHandle routeHandle;




    /**
     * 单聊 API
     * @param p2pRequest
     * @return
     */
    @RequestMapping(value = "p2pRoute", method = RequestMethod.POST)
    @ResponseBody()
    @Override
    public BaseResponse<NULLBody> p2pRoute(@RequestBody P2PReqVO p2pRequest) throws Exception {
        BaseResponse<NULLBody> res = new BaseResponse();

        try {
            //从redis 中获取获取接收消息用户的路由信息
            JIMServerResVO JIMServerResVO = accountService.loadRouteRelatedByUserId(p2pRequest.getReceiveUserId());

            //p2pRequest.getReceiveUserId()==>消息接收者的 userID
            ChatReqVO chatVO = new ChatReqVO(p2pRequest.getReceiveUserId(), p2pRequest.getMsg());
            accountService.pushMsg(JIMServerResVO, p2pRequest.getUserId(), chatVO);

            res.setCode(StatusEnum.SUCCESS.getCode());
            res.setMessage(StatusEnum.SUCCESS.getMessage());

        } catch (JIMException e) {
            res.setCode(e.getErrorCode());
            res.setMessage(e.getErrorMessage());
        }
        return res;
    }


    /**
     *群聊 API
     * @param groupReqVO
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "groupRoute", method = RequestMethod.POST)
    @ResponseBody()
    @Override
    public BaseResponse<NULLBody> groupRoute(@RequestBody ChatReqVO groupReqVO) throws Exception {
        BaseResponse<NULLBody> res = new BaseResponse();

        log.info("msg=[{}]", groupReqVO.toString());

        //获取所有的需要接收群聊消息的人列表
        // Map<userId, IM Server>
        Map<Long, JIMServerResVO> serverResVOMap = accountService.loadRouteRelated();
        for (Map.Entry<Long, JIMServerResVO> JIMServerResVOEntry : serverResVOMap.entrySet()) {
            Long userId = JIMServerResVOEntry.getKey();
            JIMServerResVO JIMServerResVO = JIMServerResVOEntry.getValue();
            if (userId.equals(groupReqVO.getUserId())) {
                //过滤掉自己
                JIMUserInfo timUserInfo = userInfoCacheService.loadUserInfoByUserId(groupReqVO.getUserId());
//                log.warn("过滤掉了发送者 userId={}", timUserInfo.toString());
                continue;
            }

            //推送消息
            ChatReqVO chatVO = new ChatReqVO(userId, groupReqVO.getMsg());
            log.info("转发userId: {} 消息: {}", userId, groupReqVO.getMsg());
            // 这里为了方便 直接使用了http 在大型IM架构中需要使用高性能RPC 服务 类似与 JSF 或者 阿里 HSF/DUBBO 等
            accountService.pushMsg(JIMServerResVO, groupReqVO.getUserId(), chatVO);

        }

        res.setCode(StatusEnum.SUCCESS.getCode());
        res.setMessage(StatusEnum.SUCCESS.getMessage());
        return res;
    }


    /**
     * 客户端下线
     * @param groupReqVO
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "offLine", method = RequestMethod.POST)
    @ResponseBody()
    @Override
    public BaseResponse<NULLBody> offLine(@RequestBody ChatReqVO groupReqVO) throws Exception {
        BaseResponse<NULLBody> res = new BaseResponse();

        JIMUserInfo timUserInfo = userInfoCacheService.loadUserInfoByUserId(groupReqVO.getUserId());

        log.info("user [{}] offline!", timUserInfo.toString());
        accountService.offLine(groupReqVO.getUserId());

        res.setCode(StatusEnum.SUCCESS.getCode());
        res.setMessage(StatusEnum.SUCCESS.getMessage());
        return res;
    }

    /**
     * 登录并获取一台 jim server
     * @param loginReqVO
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "login", method = RequestMethod.POST)
    @ResponseBody()
    @Override
    public BaseResponse<JIMServerResVO> login(@RequestBody LoginReqVO loginReqVO) throws Exception {
        BaseResponse<JIMServerResVO> res = new BaseResponse();

        //登录校验 token 验证
        StatusEnum status = accountService.login(loginReqVO);
        if (status == StatusEnum.SUCCESS) {

            // serverCache.getServerList() 从zookeeper里挑选一台客户端需访问的netty服务器
            // 通过负载均衡算法 选择一台机器 这里可以使用SPI 机制 dubbo 借鉴 dubbo
            String server = routeHandle.routeServer(serverCache.getServerList(), String.valueOf(loginReqVO.getUserId()));
            log.info("userName=[{}] route server info=[{}]", loginReqVO.getUserName(), server);

            // 解析路由信息 route
            RouteInfo routeInfo = RouteInfoParseUtil.parse(server);
            // 检测服务端活跃状态
            commonBizService.checkServerAvailable(routeInfo);

            //保存路由信息 redis
            accountService.saveRouteInfo(loginReqVO, server);
            // 构建返回body
            JIMServerResVO vo = new JIMServerResVO(routeInfo);
            res.setDataBody(vo);

        }
        res.setCode(status.getCode());
        res.setMessage(status.getMessage());

        return res;
    }

    /**
     * 注册账号
     * @param registerInfoReqVO
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "registerAccount", method = RequestMethod.POST)
    @ResponseBody()
    @Override
    public BaseResponse<RegisterInfoResVO> registerAccount(@RequestBody RegisterInfoReqVO registerInfoReqVO) throws Exception {
        BaseResponse<RegisterInfoResVO> res = new BaseResponse();

        long userId = System.currentTimeMillis();
        RegisterInfoResVO info = new RegisterInfoResVO(userId, registerInfoReqVO.getUserName());
        info = accountService.register(info);

        res.setDataBody(info);
        res.setCode(StatusEnum.SUCCESS.getCode());
        res.setMessage(StatusEnum.SUCCESS.getMessage());
        return res;
    }

    /**
     * 获取所有在线用户
     *
     * @return
     */
    @RequestMapping(value = "onlineUser", method = RequestMethod.POST)
    @ResponseBody()
    @Override
    public BaseResponse<Set<JIMUserInfo>> onlineUser() throws Exception {
        BaseResponse<Set<JIMUserInfo>> res = new BaseResponse();

        Set<JIMUserInfo> timUserInfos = userInfoCacheService.onlineUser();
        res.setDataBody(timUserInfos);
        res.setCode(StatusEnum.SUCCESS.getCode());
        res.setMessage(StatusEnum.SUCCESS.getMessage());
        return res;
    }


}
