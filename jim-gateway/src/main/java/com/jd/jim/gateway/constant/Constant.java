package com.jd.jim.gateway.constant;

/**
 * @author ：sizegang
 * @description：
 * @version:  1.0
 */
public final class Constant {


    /**
     * 账号前缀
     */
    public final static String ACCOUNT_PREFIX = "jim-account:";

    /**
     * 路由信息前缀
     */
    public final static String ROUTE_PREFIX = "jim-route:";

    /**
     * 登录状态前缀
     */
    public final static String LOGIN_STATUS_PREFIX = "login-status";


}
