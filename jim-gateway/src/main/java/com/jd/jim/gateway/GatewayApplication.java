package com.jd.jim.gateway;

import com.jd.jim.gateway.kit.ServerListListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ：sizegang
 * @description： 网关服务
 * @version: 1.0
 */
@SpringBootApplication
public class GatewayApplication implements CommandLineRunner {

    private final static Logger LOGGER = LoggerFactory.getLogger(GatewayApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
        LOGGER.info("Start jim route success!!!");
    }

    @Override
    public void run(String... args) throws Exception {
        //管理IM服务监听服务 这里使用zk进行管理 也可以使用etcd 具体使用方式 参考可 hotkey
        Thread thread = new Thread(new ServerListListener());
        thread.setName("zk-listener");
        thread.start();
    }
}