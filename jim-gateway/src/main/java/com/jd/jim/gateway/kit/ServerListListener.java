package com.jd.jim.gateway.kit;

import com.jd.jim.gateway.config.AppConfiguration;
import com.jd.jim.gateway.util.SpringBeanFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author ：sizegang
 * @description： 注册zk监听器
 * @version:  1.0
 */
public class ServerListListener implements Runnable {

    private static Logger logger = LoggerFactory.getLogger(ServerListListener.class);

    private ZkServicesWatcher zkServicesWatcher;

    private AppConfiguration appConfiguration;


    public ServerListListener() {
        zkServicesWatcher = SpringBeanFactory.getBean(ZkServicesWatcher.class);
        appConfiguration = SpringBeanFactory.getBean(AppConfiguration.class);
    }

    @Override
    public void run() {
        //注册监听服务
        zkServicesWatcher.subscribeEvent(appConfiguration.getZkRoot());

    }
}
