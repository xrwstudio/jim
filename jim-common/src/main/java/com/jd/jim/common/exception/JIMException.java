package com.jd.jim.common.exception;


import com.jd.jim.common.enums.StatusEnum;

/**
 *
 * @author: sizegang
 * @description：
 * @version:  1.0
 */
public class JIMException extends GenericException {


    public JIMException(String errorCode, String errorMessage) {
        super(errorMessage);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public JIMException(Exception e, String errorCode, String errorMessage) {
        super(e, errorMessage);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public JIMException(String message) {
        super(message);
        this.errorMessage = message;
    }

    public JIMException(StatusEnum statusEnum) {
        super(statusEnum.getMessage());
        this.errorMessage = statusEnum.message();
        this.errorCode = statusEnum.getCode();
    }

    public JIMException(StatusEnum statusEnum, String message) {
        super(message);
        this.errorMessage = message;
        this.errorCode = statusEnum.getCode();
    }

    public JIMException(Exception oriEx) {
        super(oriEx);
    }

    public JIMException(Throwable oriEx) {
        super(oriEx);
    }

    public JIMException(String message, Exception oriEx) {
        super(message, oriEx);
        this.errorMessage = message;
    }

    public JIMException(String message, Throwable oriEx) {
        super(message, oriEx);
        this.errorMessage = message;
    }


    public static boolean isResetByPeer(String msg) {
        if ("Connection reset by peer".equals(msg)) {
            return true;
        }
        return false;
    }

}
