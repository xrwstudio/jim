package com.jd.jim.common.route.algorithm.loop;

import com.jd.jim.common.enums.StatusEnum;
import com.jd.jim.common.route.algorithm.RouteHandle;
import com.jd.jim.common.exception.JIMException;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * @author: sizegang
 * @description：轮训算法 取IM 服务
 * @version:  1.0
 */
public class LoopHandle implements RouteHandle {
    private AtomicLong index = new AtomicLong();

    @Override
    public String routeServer(List<String> values,String key) {
        if (values.size() == 0) {
            throw new JIMException(StatusEnum.SERVER_NOT_AVAILABLE) ;
        }
        Long position = index.incrementAndGet() % values.size();
        if (position < 0) {
            position = 0L;
        }

        return values.get(position.intValue());
    }
}
