package com.jd.jim.common.util;

import com.jd.jim.common.enums.StatusEnum;
import com.jd.jim.common.pojo.RouteInfo;
import com.jd.jim.common.exception.JIMException;

/**
 *
 * @author: sizegang
 * @description：
 * @version:  1.0
 */
public class RouteInfoParseUtil {

    public static RouteInfo parse(String info){
        try {
            String[] serverInfo = info.split(":");
            RouteInfo routeInfo =  new RouteInfo(serverInfo[0], Integer.parseInt(serverInfo[1]),Integer.parseInt(serverInfo[2])) ;
            return routeInfo ;
        }catch (Exception e){
            throw new JIMException(StatusEnum.VALIDATION_FAIL) ;
        }
    }
}
