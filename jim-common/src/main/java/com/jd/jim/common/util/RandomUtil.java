package com.jd.jim.common.util;

import java.util.Random;
import java.util.UUID;

/**
 *
 * @author: sizegang
 * @description：
 * @version:  1.0
 */
public class RandomUtil {

    public static int getRandom() {

        double random = Math.random();
        return (int) (random * 100);
    }
}
