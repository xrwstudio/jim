package com.jd.jim.common.route.algorithm.consistenthash;

import com.jd.jim.common.route.algorithm.RouteHandle;

import java.util.List;

/**
 * @author ：sizegang
 * @description：一致性hash分配
 * @version:  1.0
 */
public class ConsistentHashHandle implements RouteHandle {
    private AbstractConsistentHash hash;

    public void setHash(AbstractConsistentHash hash) {
        this.hash = hash;
    }

    @Override
    public String routeServer(List<String> values, String key) {
        return hash.process(values, key);
    }
}
