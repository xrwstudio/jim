package com.jd.jim.common.route.algorithm;

import java.util.List;

/**
 *
 * @author: sizegang
 * @description： 分配Server执行器接口
 * @version:  1.0
 */
public interface RouteHandle {

    /**
     * 再一批服务器里进行路由
     * @param values
     * @param key
     * @return
     */
    String routeServer(List<String> values,String key) ;
}
