package com.jd.jim.common.kit;

import io.netty.channel.ChannelHandlerContext;

/**
 *
 * @author: sizegang
 * @description： 心跳处理器
 * @version:  1.0
 */
public interface HeartBeatHandler {

    /**
     * 处理心跳
     * @param ctx
     * @throws Exception
     */
    void process(ChannelHandlerContext ctx) throws Exception ;
}
