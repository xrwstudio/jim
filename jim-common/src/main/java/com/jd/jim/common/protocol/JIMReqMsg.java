package com.jd.jim.common.protocol;

/**
 * @author ：sizegang
 * @description： 发送消息实体
 * @version:  1.0
 */
public class JIMReqMsg {

    private Long requestId;
    private String reqMsg;
    private Integer type;

    public JIMReqMsg() {
    }

    public JIMReqMsg(Long requestId, String reqMsg, Integer type) {
        this.requestId = requestId;
        this.reqMsg = reqMsg;
        this.type = type;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public String getReqMsg() {
        return reqMsg;
    }

    public void setReqMsg(String reqMsg) {
        this.reqMsg = reqMsg;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
