package com.jd.jim.common.route.algorithm.random;

import com.jd.jim.common.enums.StatusEnum;
import com.jd.jim.common.exception.JIMException;
import com.jd.jim.common.route.algorithm.RouteHandle;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**

 * @author: sizegang
 * @description：   路由策略， 随机
 * @version:  1.0
 */
public class RandomHandle implements RouteHandle {

    @Override
    public String routeServer(List<String> values, String key) {
        int size = values.size();
        if (size == 0) {
            throw new JIMException(StatusEnum.SERVER_NOT_AVAILABLE) ;
        }
        int offset = ThreadLocalRandom.current().nextInt(size);

        return values.get(offset);
    }
}
