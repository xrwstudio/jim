package com.jd.jim.common.res;

/**
 *
 * @author: sizegang
 * @description： 空对象,用在泛型中,表示没有额外的请求参数或者返回参数
 * @version:  1.0
 */
public class NULLBody {
    public NULLBody() {}

    public static NULLBody create(){
        return new NULLBody();
    }
}
