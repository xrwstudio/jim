package com.jd.jim.server.api;

import com.jd.jim.server.api.vo.req.SendMsgReqVO;

/**
 *
 * @author: sizegang
 * @description：
 * @version:  1.0
 */
public interface ServerApi {

    /**
     * Push msg to client
     * @param sendMsgReqVO
     * @return
     * @throws Exception
     */
    Object sendMsg(SendMsgReqVO sendMsgReqVO) throws Exception;
}
