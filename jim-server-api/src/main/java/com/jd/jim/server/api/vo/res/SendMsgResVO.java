package com.jd.jim.server.api.vo.res;

/**
 *
 * @author: sizegang
 * @description：
 * @version:  1.0
 */
public class SendMsgResVO {
    private String msg ;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
