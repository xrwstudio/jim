package com.jd.jim.gateway.api;

import com.jd.jim.gateway.api.vo.req.P2PReqVO;
import com.jd.jim.gateway.api.vo.res.RegisterInfoResVO;
import com.jd.jim.common.res.BaseResponse;
import com.jd.jim.gateway.api.vo.req.ChatReqVO;
import com.jd.jim.gateway.api.vo.req.LoginReqVO;
import com.jd.jim.gateway.api.vo.req.RegisterInfoReqVO;

/**
 * Route Api
 * 路由
 * @author: sizegang
 * @description：
 * @version:  1.0
 */
public interface RouteApi {

    /**
     * group chat
     * 群组聊天
     * @param groupReqVO
     * @return
     * @throws Exception
     */
    Object groupRoute(ChatReqVO groupReqVO) throws Exception;

    /**
     * Point to point chat
     * 单聊
     * @param p2pRequest
     * @return
     * @throws Exception
     */
    Object p2pRoute(P2PReqVO p2pRequest) throws Exception;


    /**
     * Offline account
     * 下线
     * @param groupReqVO
     * @return
     * @throws Exception
     */
    Object offLine(ChatReqVO groupReqVO) throws Exception;

    /**
     * Login account
     * 登录
     * @param loginReqVO
     * @return
     * @throws Exception
     */
    Object login(LoginReqVO loginReqVO) throws Exception;

    /**
     * Register account
     * 注册用户
     * @param registerInfoReqVO
     * @return
     * @throws Exception
     */
    BaseResponse<RegisterInfoResVO> registerAccount(RegisterInfoReqVO registerInfoReqVO) throws Exception;

    /**
     * Get all online users
     * 获取在线用户
     * @return
     * @throws Exception
     */
    Object onlineUser() throws Exception;
}
