package com.jd.jim.gateway.api.vo.req;

import com.jd.jim.common.req.BaseRequest;

import javax.validation.constraints.NotNull;

/**
 * @author ：sizegang
 * @description：
 * @version:  1.0
 */
public class RegisterInfoReqVO extends BaseRequest {

    @NotNull(message = "用户名不能为空")
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "RegisterInfoReqVO{" +
                "userName='" + userName + '\'' +
                "} " + super.toString();
    }
}
