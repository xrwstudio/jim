package com.jd.jim.gateway.api.vo.res;

import com.jd.jim.common.pojo.RouteInfo;

import java.io.Serializable;

/**
 * @author ：sizegang
 * @description：
 * @version:  1.0
 */
public class JIMServerResVO implements Serializable {

    private String ip;
    private Integer timServerPort;
    private Integer httpPort;

    public JIMServerResVO(RouteInfo routeInfo) {
        this.ip = routeInfo.getIp();
        this.timServerPort = routeInfo.getTimServerPort();
        this.httpPort = routeInfo.getHttpPort();
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getTimServerPort() {
        return timServerPort;
    }

    public void setTimServerPort(Integer timServerPort) {
        this.timServerPort = timServerPort;
    }

    public Integer getHttpPort() {
        return httpPort;
    }

    public void setHttpPort(Integer httpPort) {
        this.httpPort = httpPort;
    }
}
