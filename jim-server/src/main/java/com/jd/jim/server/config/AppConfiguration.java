package com.jd.jim.server.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author ：sizegang
 * @description：
 * @version:  1.0
 */
@Component
public class AppConfiguration {

    @Value("${app.zk.root}")
    private String zkRoot;

    @Value("${app.zk.addr}")
    private String zkAddr;

    @Value("${app.zk.switch}")
    private boolean zkSwitch;

    @Value("${jim.server.port}")
    private int timServerPort;

    @Value("${jim.gateway.url}")
    private String gatewayUrl;

    public String getGatewayUrl() {
        return gatewayUrl;
    }

    public void setGatewayUrl(String gatewayUrl) {
        this.gatewayUrl = gatewayUrl;
    }

    @Value("${jim.heartbeat.time}")
    private int heartBeatTime;

    @Value("${app.zk.connect.timeout}")
    private int zkConnectTimeout;

    public int getZkConnectTimeout() {
        return zkConnectTimeout;
    }

    public String getZkRoot() {
        return zkRoot;
    }

    public void setZkRoot(String zkRoot) {
        this.zkRoot = zkRoot;
    }

    public String getZkAddr() {
        return zkAddr;
    }

    public void setZkAddr(String zkAddr) {
        this.zkAddr = zkAddr;
    }

    public boolean isZkSwitch() {
        return zkSwitch;
    }

    public void setZkSwitch(boolean zkSwitch) {
        this.zkSwitch = zkSwitch;
    }

    public int getTimServerPort() {
        return timServerPort;
    }

    public void setTimServerPort(int timServerPort) {
        this.timServerPort = timServerPort;
    }

    public int getHeartBeatTime() {
        return heartBeatTime;
    }

    public void setHeartBeatTime(int heartBeatTime) {
        this.heartBeatTime = heartBeatTime;
    }
}
