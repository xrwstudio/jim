package com.jd.jim.server.init;

import com.jd.jim.server.handle.JIMServerHandle;
import com.jd.jim.common.protocol.ObjDecoder;
import com.jd.jim.common.protocol.ObjEncoder;
import com.jd.jim.common.protocol.JIMReqMsg;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.timeout.IdleStateHandler;

/**
 * @author ：sizegang
 * @description： JIM Server  初始化器
 * @version:  1.0
 */
public class JIMServerInitializer extends ChannelInitializer<Channel> {


    // netty消息处理器
    private final JIMServerHandle timServerHandle = new JIMServerHandle();

    @Override
    protected void initChannel(Channel ch) throws Exception {

        ch.pipeline()
                // 基于Netty 事件驱动开发模式
                // 20秒没有收到客户端发送消息或心跳就触发读空闲，执行JIMServerHandle的 userEventTriggered 方法关闭客户端连接
                .addLast(new IdleStateHandler(20, 0, 0))
                // 自定义 Google protoBuf 编解码
                /**
                 * protocol buffers 是一种语言无关、平台无关、可扩展的序列化结构数据的方法，它可用于（数据）通信协议、数据存储等。
                 * Protocol Buffers 是一种灵活，高效，自动化机制的结构数据序列化方法－可类比 XML，但是比 XML 更小（3 ~ 10倍）、更快（20 ~ 100倍）、更为简单。
                 */
                .addLast(new ObjEncoder(JIMReqMsg.class))
                .addLast(new ObjDecoder(JIMReqMsg.class))
                .addLast(timServerHandle);
    }
}
