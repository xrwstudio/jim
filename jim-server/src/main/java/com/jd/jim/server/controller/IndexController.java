package com.jd.jim.server.controller;

import com.jd.jim.common.enums.StatusEnum;
import com.jd.jim.common.res.BaseResponse;
import com.jd.jim.server.api.ServerApi;
import com.jd.jim.server.api.vo.req.SendMsgReqVO;
import com.jd.jim.server.api.vo.res.SendMsgResVO;
import com.jd.jim.server.server.JIMServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author ：sizegang
 * @description：
 * @version:  1.0
 */
@Controller
@RequestMapping("/")
public class IndexController implements ServerApi {

    @Autowired
    private JIMServer jimServer;


    /**
     * @param sendMsgReqVO
     * @return
     */
    @Override
    @RequestMapping(value = "sendMsg", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse<SendMsgResVO> sendMsg(@RequestBody SendMsgReqVO sendMsgReqVO) {
        BaseResponse<SendMsgResVO> res = new BaseResponse();
        jimServer.sendMsg(sendMsgReqVO);
        SendMsgResVO sendMsgResVO = new SendMsgResVO();
        sendMsgResVO.setMsg("OK");
        res.setCode(StatusEnum.SUCCESS.getCode());
        res.setMessage(StatusEnum.SUCCESS.getMessage());
        res.setDataBody(sendMsgResVO);
        return res;
    }

}
