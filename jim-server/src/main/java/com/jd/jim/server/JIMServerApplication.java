package com.jd.jim.server;

import com.jd.jim.server.config.AppConfiguration;
import com.jd.jim.server.kit.RegistryZK;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.InetAddress;

/**
 *
 */
@SpringBootApplication
public class JIMServerApplication implements CommandLineRunner {

    private final static Logger LOGGER = LoggerFactory.getLogger(JIMServerApplication.class);

    @Autowired
    private AppConfiguration appConfiguration;

    @Value("${server.port}")
    private int httpPort;

    public static void main(String[] args) {
        SpringApplication.run(JIMServerApplication.class, args);
        LOGGER.info("Start jim server success!!!");
    }

    @Override
    public void run(String... args) throws Exception {
        //获得本机IP
        String addr = InetAddress.getLocalHost().getHostAddress();
        /**
         * 异步注册将服务注册到zookeeper中
         */
        Thread thread = new Thread(new RegistryZK(addr, appConfiguration.getTimServerPort(), httpPort));
        thread.setName("registry-zk");
        thread.start();
    }
}