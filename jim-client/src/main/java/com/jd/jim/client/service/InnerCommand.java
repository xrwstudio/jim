package com.jd.jim.client.service;

/**
 *
 * @author: sizegang
 * @description：
 * @version:  1.0
 */
public interface InnerCommand {

    /**
     * 执行
     * @param msg 
     */
    void process(String msg) ;
}
