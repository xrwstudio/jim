package com.jd.jim.client.service;

/**
 *
 * @author: sizegang
 * @description：
 * @version:  1.0
 */
public interface CustomMsgHandleListener {

    /**
     * 消息回调
     * @param msg
     */
    void handle(String msg);
}
