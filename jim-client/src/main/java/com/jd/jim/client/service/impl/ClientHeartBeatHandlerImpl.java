package com.jd.jim.client.service.impl;

import com.jd.jim.client.client.JIMClient;
import com.jd.jim.client.thread.ContextHolder;
import com.jd.jim.common.kit.HeartBeatHandler;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ：sizegang
 * @description：
 * @version:  1.0
 */
@Service
@Slf4j
public class ClientHeartBeatHandlerImpl implements HeartBeatHandler {

    @Autowired
    private JIMClient timClient;

    @Override
    public void process(ChannelHandlerContext ctx) throws Exception {
        //重连 设置重连态 防止被system.exit()
        ContextHolder.setReconnect(true);
        timClient.reconnect();
    }


}
