package com.jd.jim.client.service;

import com.jd.jim.client.vo.req.GroupReqVO;
import com.jd.jim.client.vo.req.LoginReqVO;
import com.jd.jim.client.vo.req.P2PReqVO;
import com.jd.jim.client.vo.res.OnlineUsersResVO;
import com.jd.jim.client.vo.res.JIMServerResVO;

import java.util.List;

/**
 *
 * @author: sizegang
 * @description：
 * @version:  1.0
 */
public interface RouteRequest {

    /**
     * 群组消息
     * @param groupReqVO 消息
     * @throws Exception
     */
    void sendGroupMsg(GroupReqVO groupReqVO) throws Exception;


    /**
     * 私聊
     * @param p2PReqVO
     * @throws Exception
     */
    void sendP2PMsg(P2PReqVO p2PReqVO)throws Exception;

    /**
     * 获取服务器
     * @return 服务ip+port
     * @param loginReqVO
     * @throws Exception
     */
    JIMServerResVO.ServerInfo getTIMServer(LoginReqVO loginReqVO) throws Exception;

    /**
     * 获取所有在线用户
     * @return
     * @throws Exception
     */
    List<OnlineUsersResVO.DataBodyBean> onlineUsers()throws Exception ;


    void offLine() ;

}
