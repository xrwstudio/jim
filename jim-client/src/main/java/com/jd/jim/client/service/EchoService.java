package com.jd.jim.client.service;

/**
 *
 * @author: sizegang
 * @description： 打印服务
 * @version:  1.0
 */
public interface EchoService {

    /**
     * echo msg to terminal
     * @param msg message
     * @param replace
     */
    void echo(String msg, Object... replace) ;
}
