package com.jd.jim.client.vo.req;

import com.jd.jim.common.req.BaseRequest;

import javax.validation.constraints.NotNull;

/**
 * @author ：sizegang
 * @description：
 * @version:  1.0
 */
public class StringReqVO extends BaseRequest {

    @NotNull(message = "msg 不能为空")
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
