package com.jd.jim.client.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author ：sizegang
 * @description：
 * @version:  1.0
 */
@Component
public class AppConfiguration {

    @Value("${jim.user.id}")
    private Long userId;

    @Value("${jim.user.userName}")
    private String userName;

    @Value("${jim.msg.logger.path}")
    private String msgLoggerPath;

    @Value("${jim.heartbeat.time}")
    private int heartBeatTime;

    @Value("5")
    private int errorCount;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMsgLoggerPath() {
        return msgLoggerPath;
    }

    public void setMsgLoggerPath(String msgLoggerPath) {
        this.msgLoggerPath = msgLoggerPath;
    }


    public int getHeartBeatTime() {
        return heartBeatTime;
    }

    public void setHeartBeatTime(int heartBeatTime) {
        this.heartBeatTime = heartBeatTime;
    }

    public int getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(int errorCount) {
        this.errorCount = errorCount;
    }
}
