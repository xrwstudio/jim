package com.jd.jim.client.init;

import com.jd.jim.client.config.AppConfiguration;
import com.jd.jim.client.handle.JIMClientHandle;
import com.jd.jim.common.protocol.ObjDecoder;
import com.jd.jim.common.protocol.ObjEncoder;
import com.jd.jim.common.protocol.JIMReqMsg;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.timeout.IdleStateHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author ：sizegang
 * @description： 客户端初始化
 * @version:  1.0
 */
public class JIMClientHandleInitializer extends ChannelInitializer<Channel> {

    private final JIMClientHandle TIMClientHandle = new JIMClientHandle();

    @Override
    protected void initChannel(Channel ch) throws Exception {
        ch.pipeline()
                // 心跳
                // 基于Netty 事件驱动开发模式
                // 15 秒客户端没给服务端主动发送消息就触发写空闲，执行JIMClientHandle的 userEventTriggered 方法给服务端发送一次心跳
                .addLast(new IdleStateHandler(0, 15, 0))
                // 自定义 Google protoBuf 编解码
                /**
                 * protocol buffers 是一种语言无关、平台无关、可扩展的序列化结构数据的方法，它可用于（数据）通信协议、数据存储等。
                 * Protocol Buffers 是一种灵活，高效，自动化机制的结构数据序列化方法－可类比 XML，但是比 XML 更小（3 ~ 10倍）、更快（20 ~ 100倍）、更为简单。
                 */
                .addLast(new ObjEncoder(JIMReqMsg.class))
                .addLast(new ObjDecoder(JIMReqMsg.class))
                .addLast(TIMClientHandle)
        ;
    }
}
