package com.jd.jim.client.service.impl;

import com.jd.jim.client.service.CustomMsgHandleListener;
import com.jd.jim.client.service.MsgLogger;
import com.jd.jim.client.util.SpringBeanFactory;

/**
 * 自定义收到消息回调
 *
 * @author: sizegang
 * @description：
 * @version:  1.0
 */
public class MsgCallBackListener implements CustomMsgHandleListener {


    private MsgLogger msgLogger ;

    public MsgCallBackListener() {
        this.msgLogger = SpringBeanFactory.getBean(MsgLogger.class) ;
    }

    @Override
    public void handle(String msg) {
        msgLogger.log(msg) ;
    }
}
