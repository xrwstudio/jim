package com.jd.jim.server.test;

import com.jd.jim.client.JIMClientApplication;
import com.jd.jim.client.service.RouteRequest;
import com.jd.jim.client.vo.req.LoginReqVO;
import com.jd.jim.client.vo.res.JIMServerResVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author: sizegang
 * @description：
 * @version:  1.0
 */
@SpringBootTest(classes = JIMClientApplication.class)
@RunWith(SpringRunner.class)
public class RouteTest {

    private final static Logger LOGGER = LoggerFactory.getLogger(RouteTest.class);

    @Value("${jim.user.id}")
    private long userId;

    @Value("${jim.user.userName}")
    private String userName;

    @Autowired
    private RouteRequest routeRequest ;

    @Test
    public void test() throws Exception {
        LoginReqVO vo = new LoginReqVO(userId,userName) ;
        JIMServerResVO.ServerInfo timServer = routeRequest.getTIMServer(vo);
        LOGGER.info("timServer=[{}]",timServer.toString());
    }
}
